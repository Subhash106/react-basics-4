import { useState, useEffect } from 'react';

const useCounter = (start = 0, step = 1, direction = 'up') => {
    const [counter, setCounter] = useState(start);

    useEffect(() => {
        const interval = setInterval(() => {
            if (direction === 'up') {
                setCounter(prevState => prevState + step);
            } else {
                setCounter(prevState => prevState - step);
            }
        }, 1000)

        return () => {
            clearInterval(interval);
        }
    }, [start, step, direction])

    return counter;
}

export default useCounter;