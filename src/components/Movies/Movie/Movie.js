import React from "react";

import styles from "./Movie.module.css";
import Card from "../../UI/Card/Card";

const Movie = (props) => {
  return (
    <Card className={styles.movie}>
      <h2>{props.name}</h2>
      <p>{props.openingText}</p>
    </Card>
  );
};

export default Movie;
