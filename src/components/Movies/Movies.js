import React from "react";

import styles from "./Movies.module.css";
import Movie from './Movie/Movie';
import Card from "../UI/Card/Card";

const Movies = (props) => {

    let movies = 'Click button to fetch movies!';

    if (props.movies.length > 0) {
        movies = props.movies.map(movie => <Movie key={movie.id} name={movie.name} openingText={movie.openingText} />);
    }

  return <Card className={styles.movies}>
      {movies}
  </Card>;
};

export default Movies;
