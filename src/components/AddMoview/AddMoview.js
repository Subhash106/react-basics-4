import React, { useState } from "react";

import styles from "./AddMoview.module.css";
import Button from "../UI/Button/Button";
import Card from "../UI/Card/Card";

const AddMoview = () => {
  const [title, setTitle] = useState("");
  const [openingText, setOpeningText] = useState("");
  const [releaseDate, setReleaseDate] = useState("");

  const addMovieHandler = (event) => {
    event.preventDefault();

    const data = {
      title: title,
      openingText: openingText,
      releaseDate: releaseDate,
    };

    fetch("https://basic-react-a8d88-default-rtdb.firebaseio.com/movies.json", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        console.log(data);
      });

      setTitle('');
      setOpeningText('');
      setReleaseDate('');
  };

  const titleChangesHandler = (event) => {
    setTitle(event.target.value);
  };

  const openingTextChangesHandler = (event) => {
    setOpeningText(event.target.value);
  };

  const releaseDateChangesHandler = (event) => {
    setReleaseDate(event.target.value);
  };

  return (
    <Card className={styles.addMoview}>
      <form onSubmit={addMovieHandler}>
        <div className={styles.controls}>
          <div className={styles.control}>
            <label>Title</label>
            <input type="text" onChange={titleChangesHandler} value={title} />
          </div>

          <div className={styles.control}>
            <label>Opening Text</label>
            <textarea
              onChange={openingTextChangesHandler}
              value={openingText}
            />
          </div>

          <div className={styles.control}>
            <label>Release Date</label>
            <input
              type="date"
              onChange={releaseDateChangesHandler}
              value={releaseDate}
            />
          </div>
        </div>

        <Button>Add Movie</Button>
      </form>
    </Card>
  );
};

export default AddMoview;
