import Card from '../UI/Card/Card';
import useCounter from '../../store/use-counter';

const BackwardCounter = () => {

    const counter = useCounter(10, 5, 'down');

    return <Card>
        {counter}
    </Card>
}

export default BackwardCounter;