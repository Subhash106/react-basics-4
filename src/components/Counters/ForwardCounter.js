import Card from '../UI/Card/Card';
import useCounter from '../../store/use-counter';

const ForwardCounter = () => {

    const counter = useCounter(10, 5, 'up');

    return <Card>
        {counter}
    </Card>
}

export default ForwardCounter;