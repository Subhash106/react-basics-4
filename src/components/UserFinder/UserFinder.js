import React, { useState, useEffect, Component } from "react";

import styles from "./UserFinder.module.css";
import Users from "../Users/Users";

class UserFinder extends Component {
  constructor() {
    super();

    this.state = {
      users: [
        { id: "i1", name: "Subhash", age: 20 },
        { id: "i2", name: "Suresh", age: 21 },
        { id: "i3", name: "Ramesh", age: 24 },
        { id: "i4", name: "Kariya", age: 22 },
        { id: "i5", name: "Rubesh", age: 28 },
      ],
      query: "",
    };
  }

  inputChangeHandler = (event) => {
    this.setState({ query: event.target.value });
  };

  componentDidUpdate(prevProps, prevState) {
    console.log("componentDidUpdate called!");
    if (prevState.query !== this.state.query) {
      this.setState({
        users: this.state.users.filter((user) =>
          user.name.includes(this.state.query)
        ),
      });
    }
  }

  render() {
    return (
      <div className={styles.userFinder}>
        <input
          type="text"
          name="query"
          value={this.state.query}
          onChange={this.inputChangeHandler.bind(this)}
        />

        <Users users={this.state.users} />
      </div>
    );
  }
}

// const UserFinder = () => {
//   const users = [
//     { id: "i1", name: "Subhash", age: 20 },
//     { id: "i2", name: "Suresh", age: 21 },
//     { id: "i3", name: "Ramesh", age: 24 },
//     { id: "i4", name: "Kariya", age: 22 },
//     { id: "i5", name: "Rubesh", age: 28 },
//   ];

//   const [query, setQuery] = useState("");

//   const inputChangeHandler = (event) => {
//     setQuery(event.target.value);
//   };

//   useEffect(() => {
//       users.filter(user => user.name.includes(query))
//   }, [query]);

//   return (
//     <div className={styles.userFinder}>
//       <input
//         type="text"
//         name="query"
//         value={query}
//         onChange={inputChangeHandler}
//       />

//       <Users users={users} />
//     </div>
//   );
// };

export default UserFinder;
