import React from 'react';

import styles from './Task.module.css';
import Card from '../../UI/Card/Card';

const Task = (props) => {
    return <Card className={styles.task}>
        {props.title}
    </Card>
}

export default Task;