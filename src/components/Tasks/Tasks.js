import React, { useState, useEffect, Fragment } from "react";

import styles from "./Tasks.module.css";
import Card from "../UI/Card/Card";
import Task from "./Task/Task";
import Button from "../UI/Button/Button";

const Tasks = (props) => {
  const [tasks, setTasks] = useState([]);

  const [title, setTitle] = useState("");

  const fetchTasks = () => {
    fetch("https://basic-react-a8d88-default-rtdb.firebaseio.com/tasks.json")
      .then((response) => response.json())
      .then((data) => {
        const formatedTasks = [];

        for (const key in data) {
          formatedTasks.push({
            id: key,
            title: data[key].title,
          });
        }

        setTasks(formatedTasks);
      });
  }

  useEffect(() => {
    fetchTasks();
  }, []);

  const changeTitleHandler = (event) => {
    setTitle(event.target.value);
  };

  const submitTaskHandler = (event) => {
    event.preventDefault();

    const data = {
      title: title,
    };

    fetch("https://basic-react-a8d88-default-rtdb.firebaseio.com/tasks.json", {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    });

    setTitle('');

    fetchTasks();
  };

  return (
    <Fragment>
      <Card>
        <form onSubmit={submitTaskHandler}>
          <input type="text" value={title} onChange={changeTitleHandler} />
          <Button>Save Task</Button>
        </form>
      </Card>
      <Card className={styles.tasks}>
        {tasks.map((task) => (
          <Task key={task.id} title={task.title} />
        ))}
      </Card>
    </Fragment>
  );
};

export default Tasks;
