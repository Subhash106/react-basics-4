import React, { Component } from "react";

import styles from "./Users.module.css";
import User from "./User/User";
import Button from "../UI/Button/Button";

class Users extends Component {
  constructor() {
    super();
    this.state = {
      showUsersList: true,
    };
  }

  toggleUsersListHandler = () => {
    this.setState((prevState) => {
      return { showUsersList: !prevState.showUsersList };
    });
  };

  render() {
    const usersList = this.props.users.map((user) => (
      <User key={user.id} name={user.name} age={user.age} />
    ));

    return (
      <div className={styles.users}>
        <Button onClick={this.toggleUsersListHandler.bind(this)}>Toggle Users</Button>
        <ul>{this.state.showUsersList && usersList}</ul>
      </div>
    );
  }
}

// const Users = () => {
//   const [showUsersList, setShowUsersList] = useState(true);

//   const users = [
//     { id: "i1", name: "Subhash", age: 20 },
//     { id: "i2", name: "Suresh", age: 21 },
//     { id: "i3", name: "Ramesh", age: 24 },
//     { id: "i4", name: "Kariya", age: 22 },
//     { id: "i5", name: "Rubesh", age: 28 },
//   ];

//   const toggleUsersListHandler = () => {
//     setShowUsersList((prevState) => !prevState);
//   };

//   const usersList = users.map((user) => (
//     <User key={user.id} name={user.name} age={user.age} />
//   ));

//   return (
//     <div className={styles.users}>
//       <Button onClick={toggleUsersListHandler}>Toggle Users</Button>
//       <ul>{showUsersList && usersList}</ul>
//     </div>
//   );
// };

export default Users;
