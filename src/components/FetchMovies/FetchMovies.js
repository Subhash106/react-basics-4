import React from "react";

import styles from "./FetchMovies.module.css";
import Button from "../UI/Button/Button";
import Card from "../UI/Card/Card";

const FetchMovies = (props) => {
  return (
    <Card className={styles.fetchMovies}>
      <Button onClick={props.onFetchMovies}>Fetch Movie</Button>
    </Card>
  );
};

export default FetchMovies;
