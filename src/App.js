import "./App.css";
import Movies from "./components/Movies/Movies";
import FetchMovies from "./components/FetchMovies/FetchMovies";
import { useState, useEffect, useCallback } from "react";
import AddMoview from "./components/AddMoview/AddMoview";
import Tasks from "./components/Tasks/Tasks";

function App() {
  const [movies, setMovies] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(false);

  const fetchMoviesHandler = useCallback(async () => {
    try {
      setError(false);
      setIsLoading(true);
      const response = await fetch("https://basic-react-a8d88-default-rtdb.firebaseio.com/movies.json", {
          headers: {
              'Access-Control-Allow-Origin': '*'
          }
      });

      const data = await response.json();

      const formatedMovies = [];

      for (const key in data) {
        formatedMovies.push({
              id: key,
              name: data[key].title,
              openingText: data[key].openingText,
              releaseDate: data[key].releaseDate
          })
      }

      setMovies(formatedMovies);

      setIsLoading(false);
    } catch (error) {
      setError(true);
    }
  }, []);

  useEffect(() => {
    fetchMoviesHandler();
  }, [fetchMoviesHandler]);

  return (
    <div className="App">
    <Tasks />
      <AddMoview />
      <FetchMovies onFetchMovies={fetchMoviesHandler} />
      {!isLoading && <Movies movies={movies} />}
      {isLoading && !error && <p>Loading...</p>}
      {error && <p>Something went wrong!</p>}
    </div>
  );
}

export default App;
